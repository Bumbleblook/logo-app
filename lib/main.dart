import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() {
  runApp(AniApp());
}

class AniApp extends StatefulWidget {
  _AniAppState createState() => _AniAppState();
}

class _AniAppState extends State<AniApp> with SingleTickerProviderStateMixin {
  // This widget is the root of your application.
  Animation<double> animation;
  AnimationController controller;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 3), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });
    animation.addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);
}

class AnimatedLogo extends AnimatedWidget {
  // static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  // static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Stack(children: [
      AnimatedSwitcher(
        duration: const Duration(seconds: 1),
        transitionBuilder: (Widget child, Animation<double> animation) {
          return FadeTransition(
            child: FlutterLogo(),
            opacity: animation,
          );
        },
      ),
      AnimatedSwitcher(
          duration: const Duration(seconds: 2),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return SizeTransition(
              child: FlutterLogo(),
              sizeFactor: animation,
            );
          }),
    ]);
  }
}
